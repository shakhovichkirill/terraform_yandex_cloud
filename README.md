# terraform_yandex_cloud

## Настройка Gitlab CI для Terraform
Настройка Gitlab CI для Terraform реализована в рамках отдельного проекта: https://gitlab.com/shakhovichkirill/terraform_yandex_cloud
Для использования Gitlab CI в рамках развертывания Managed Service for Kubernetes в Yandex Cloud с помощью Terraform необходимо:
1. Реализовать загрузку состояний в S3 хранилище. Для этого выполнить действия согласно документации [инструкция](https://yandex.cloud/ru/docs/tutorials/infrastructure-management/terraform-state-storage)
2. Реализовать блокировку состояний Terraform c помощью YDB. Алгоритм действий описан в документации [инструкция](https://yandex.cloud/ru/docs/tutorials/infrastructure-management/terraform-state-lock)
3. Также для использования pipeline необходима передача переменных для авторизации:

   AWS_ACCESS_KEY_ID данная переменая равняется id access-key

   AWS_SECRET_ACCESS_KEY данная переменная равняется secret access-key

   команда для создания access-key сервисного аккаунта
   ```
   yc iam access-key create --service-account-name teradmin
   ```

4. Также для provider yandex terraform потребуются данные для авторизации на основе токена или ключа. Реализация  на основе файла ключа:
   Соаздадим переменную YC_KEY в vars проекта terraform, значение переменной можно получить из след. команды      создания ключа для сервисного аккаунта:
   ```
   yc iam key create --service-account-name teradmin --output sa-key.json
   ```