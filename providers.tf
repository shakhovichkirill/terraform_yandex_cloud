terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"


  backend "s3" { 
    endpoint = "https://storage.yandexcloud.net"
        
    region   = "ru-central1"
    bucket = "tfstate5312"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true 
    skip_s3_checksum            = true

    dynamodb_table    = "tfstate5312"
    dynamodb_endpoint = "https://docapi.serverless.yandexcloud.net/ru-central1/b1gfchicoir964jvcft6/etn55duqvdq0isnc66hs"

    key = "terraform.tfstate"
  }
}

provider "yandex" {
  zone = "ru-central1-d"
  cloud_id = "b1gfchicoir964jvcft6"
  folder_id = "b1g75hob5pvq0360545e"
  service_account_key_file = "/tmp/sa-key.json"
}